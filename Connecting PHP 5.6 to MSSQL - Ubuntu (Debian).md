[http://www.freetds.org/](http://www.freetds.org/)

1. ``` sudo apt-get install php5.6-sybase freetds-common libsybdb5 ```
2. ``` sudo nano /etc/freetds/freetds.conf ```
```
    [servername]
        host = 127.0.0.1
        port = 1433
        tds version = 8.0
```
3. Use [https://github.com/realestateconz/MssqlBundle](https://github.com/realestateconz/MssqlBundle) for symfony